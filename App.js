import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  ScrollView,
} from "react-native";
import quote from "./components/quote";

export default function App() {
  const [history, sethistory] = useState([]);
  const presshandler = (text) => {
    const s1=`So you decide to take the ${text} pill... :)`;
    sethistory((prevHis) => {
      return [s1, ...prevHis];
    });
  };
  return (
    <View style={styles.container}>
      <Image style={styles.img} source={require("./assets/morpheus.jpg")} />
      <View style={styles.touchH}>
        <TouchableHighlight style={styles.button} onPress={()=>presshandler('red')}>
          <Text style={styles.buttonText}>Red Pill</Text>
        </TouchableHighlight>
        <TouchableHighlight style={styles.button1} onPress={()=>presshandler('blue')}>
          <Text style={styles.buttonText}>Blue Pill</Text>
        </TouchableHighlight>
      </View>
      <View></View>
      <ScrollView style={styles.quoteView}>
      <Text style={styles.his}>History</Text>
        {history.map((item) => {
          return <Text>{item} </Text>;
        })}
        <Text style={styles.quoteHead}>Quotes</Text>
        {quote.map((item) => {
          return <Text style={styles.quotes}>{item}</Text>;
        })}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  img: {
    alignContent: "center",
    marginTop: 30,
    marginBottom:10,
  },
  txt: {
    textAlign: "center",
    marginVertical: 5,
    fontSize: 18,
    lineHeight: 22,
  },
  touchH: {
    flexDirection: "row",
    justifyContent: "center",
  },
  button: {
    width: 120,
    height: 50,
    margin: 5,
    borderRadius: 5,
    backgroundColor: "#F44336",
    justifyContent: "center",
    alignItems: "center",
  },
  button1: {
    width: 120,
    height: 50,
    margin: 5,
    borderRadius: 5,
    backgroundColor: "#1E88E5",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
  },
  quoteView: {
    flex: 1,
    marginTop: 5,
  },
  quoteHead: {
    fontSize: 20,
    fontWeight: "bold",
    marginVertical: 5,
  },
  quotes: {
    marginVertical: 5,
    fontSize: 16,
    lineHeight: 18,
  },
  his:{
    fontWeight:'bold',
    fontSize:16,
  }
});
